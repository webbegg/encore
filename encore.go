package encore

import (
	"fmt"

	"github.com/jinzhu/gorm"
	"github.com/labstack/echo"
)

type Encore struct {
	config appConfig
	Server *echo.Echo
	Apps   []*Application
	Db     *gorm.DB
}

// Inits encore
func New() (core *Encore) {
	core = &Encore{}

	core.config.Get()
	core.InitService()

	return core
}

// Starts encore services
func (core *Encore) RegisterApp(app *Application) {
	core.Apps = append(core.Apps, app)

	fmt.Println("⇨ [app." + app.Name + "] Registering ...")

	core.Migrate(app)
	fmt.Println("⇨ [app." + app.Name + "] DB migrated")

	fmt.Println("⇨ [app." + app.Name + "] Routes registered")
	core.RegisterRoutes(app)
}
