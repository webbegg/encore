package encore

import "github.com/jinzhu/gorm"

// Application ...
type Application struct {
	Context     *Encore
	Name        string
	Description string
	Migrate     func(db *gorm.DB) bool
	Router      Router
}
