package encore

type Model interface {
	TableName() string                   // Table name in database
	All() []interface{}                  // Returns all rows
	Find(id int) interface{}             // Find row
	Create(item interface{}) interface{} // Create row
	Edit(item interface{}) interface{}   // Update row
	Delete(item interface{}) bool        // Delete row
}
