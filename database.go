package encore

import (
	"fmt"

	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/mssql"
	_ "github.com/jinzhu/gorm/dialects/mysql"
	_ "github.com/jinzhu/gorm/dialects/postgres"
	_ "github.com/jinzhu/gorm/dialects/sqlite"
)

func (core *Encore) Connection() *gorm.DB {
	var conn, err = gorm.Open(core.config.Db.Dialect, core.config.Db.Connection)
	if err != nil {
		fmt.Println(err)
		conn = nil
	}
	return conn
}

func (core *Encore) Migrate(app *Application) {
	var db = core.Connection()
	defer db.Close()

	app.Migrate(db)
}

func (core *Encore) Find(out interface{}) {
	var db = core.Connection()
	defer db.Close()
	db.Find(out)
}

func (core *Encore) Update(attrs ...interface{}) {
	var db = core.Connection()
	defer db.Close()
	db.Update(attrs)
}
