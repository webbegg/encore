package encore

import (
	"encoding/json"
	"fmt"
	"os"

	"github.com/labstack/gommon/log"
)

type srvConfig struct {
	Port   string    `json:"port"`
	Cors   bool      `json:"cors"`
	Static staticSrv `json:static`
}
type dbConfig struct {
	Dialect    string `json:"dialect"`
	Connection string `json:"connection"`
}
type staticSrv struct {
	Active bool   `json:"active"`
	Prefix string `json:"prefix"`
	Root   string `json:"root"`
}

type appConfig struct {
	Name string    `json:"name"`
	Srv  srvConfig `json:"service"`
	Db   dbConfig  `json:"database"`
}

func (c *appConfig) Get() {
	configFile, err := os.Open("config.json")
	defer configFile.Close()
	if err != nil {
		fmt.Println(err.Error())
	}
	jsonParser := json.NewDecoder(configFile)
	err = jsonParser.Decode(&c)
	if err != nil {
		log.Fatal(err)
	}
}
