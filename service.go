package encore

import (
	"fmt"

	"github.com/labstack/echo"
	"github.com/labstack/echo/middleware"
)

func (core *Encore) InitService() {
	core.Server = echo.New()
	core.Server.HideBanner = true
	core.Server.Use(middleware.Logger())
	core.Server.Use(middleware.Recover())
	core.Server.Use(middleware.AddTrailingSlash())
	if core.config.Srv.Cors {
		core.Server.Use(middleware.CORS())
	}
	if core.config.Srv.Static.Active {
		fmt.Println("⇨ Serving static files on " + core.config.Srv.Static.Prefix + " from " + core.config.Srv.Static.Root)
		core.Server.Static(core.config.Srv.Static.Prefix, core.config.Srv.Static.Root)
	}
}

func (core *Encore) StartService() {
	core.Server.Logger.Fatal(core.Server.Start(":" + core.config.Srv.Port))
}

func (core *Encore) RegisterRoutes(app *Application) {
	for _, route := range app.Router.Routes {
		var path = app.Router.Prefix + route.Path
		if app.Router.Prefix != "" && route.Path == "/" {
			path = app.Router.Prefix
		}
		core.Server.Add(route.Method, path, route.Handler)
		fmt.Println("  [" + route.Method + "] " + path)
	}
}
