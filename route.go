package encore

import "github.com/labstack/echo"

type Route struct {
	Name    string
	Method  string
	Path    string
	Handler func(ctx echo.Context) error
}
