package encore

import "github.com/labstack/echo"

type Resource interface {
	All(c echo.Context) error
	Show(c echo.Context) error
	Create(c echo.Context) error
	Update(c echo.Context) error
	Delete(c echo.Context) error
}
