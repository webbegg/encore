## CONFIG

```json
{
  "name": "AppName",
  "service": {
    "port": "8015",
    "cors": true
  },
  "database": {
    "dialect": "mssql",
    "connection": "sqlserver://username:password@localhost:1433?database=dbname"
  }
}
```

## MAIN

```go
package main

import "webbegg.com/encore"

func main() {
	var e = encore.New()

	e.RegisterApp(tasks.tasks)

	e.StartService()
}
```

## APP MODULE

```go
// App ...
var App encore.Application

// New ...
func New(context *encore.Encore) *encore.Application {
	App = encore.Application{}
	App.Context = context
	App.Name = "tasks"
	App.Description = "tasks app"

	App.Migrate = func(db *gorm.DB) bool {
		return true
	}

	// Routes
	App.Router.GET("documents", Documents)

	return &App
}
```
