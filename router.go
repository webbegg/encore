package encore

import "github.com/labstack/echo"

type Router struct {
	Prefix string
	Routes []Route
}

func (router *Router) SetPrefix(prefix string) {
	router.Prefix = "/" + prefix
}

func (router *Router) Get(path string, handle func(ctx echo.Context) error) {
	router.Routes = append(router.Routes, Route{Path: path, Method: "GET", Handler: handle})
}

func (router *Router) Post(path string, handle func(ctx echo.Context) error) {
	router.Routes = append(router.Routes, Route{Path: path, Method: "POST", Handler: handle})
}

func (router *Router) Put(path string, handle func(ctx echo.Context) error) {
	router.Routes = append(router.Routes, Route{Path: path, Method: "PUT", Handler: handle})
}

func (router *Router) Delete(path string, handle func(ctx echo.Context) error) {
	router.Routes = append(router.Routes, Route{Path: path, Method: "DELETE", Handler: handle})
}

func (router *Router) Resource(path string, resource Resource) {
	router.Routes = append(router.Routes, Route{Path: path, Method: "GET", Handler: resource.All})
	router.Routes = append(router.Routes, Route{Path: path + "/:id", Method: "GET", Handler: resource.Show})
	router.Routes = append(router.Routes, Route{Path: path, Method: "POST", Handler: resource.Create})
	router.Routes = append(router.Routes, Route{Path: path + "/:id", Method: "PUT", Handler: resource.Update})
	router.Routes = append(router.Routes, Route{Path: path + "/:id", Method: "PATCH", Handler: resource.Update})
	router.Routes = append(router.Routes, Route{Path: path + "/:id", Method: "DELETE", Handler: resource.Delete})
}
